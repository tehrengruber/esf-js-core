var Esf = require('./esf-core.js'),
    random = function () { return Math.random(); };

module.exports = {
    /**
     * Test class definition
     * @param test
     */
    testClassDefinition: function (test) {
        var rand;

        // Define class
        Esf.define('A', {
            a: null,

            b: 'test',

            c: null,

            constructor: function (a) {
                // Save var
                this.a = a;
            },
            foo: function (b) {
                return b;
            },
            set: function (c) {
                this.c = c;
            }
        });

        // Create object
        var obj = Esf.create('A', rand = random());

        // Test attributes
        test.equals(obj.a, rand, "Object attribute wasn't set by constructor");
        test.equals(obj.b, 'test', "Defined attribute not found in object");

        // Test method invocation and return value
        test.equals(obj.foo(rand = random()), rand, "Object method invocation didn't returned expected value");

        // Test scope of methods
        obj.set(rand = random())
        test.equals(rand, obj.c, "Scope of method wasn't the object");

        // Test global creation
        var obj = new A(rand = random());
        test.equals(obj.a, rand, "Class was not mapped to global scope");

        test.done();
    },

    /**
     * Test simple class inheritance
     * @param test
     */
    testClassInheritance: function (test) {
        var rand;

        // Define class
        Esf.define('B', {
            a: null,

            b: 'test',

            c: null,

            constructor: function (a) {
                // Save var
                this.a = a;
            },
            foo: function (b) {
                return b;
            },
            set: function (c) {
                this.c = c;
            }
        });

        // Define class
        Esf.define('C', {
            d: 'test',

            constructor: function (a) {
                this.callParent(a);
            },
            foo: function (b) {
                return this.callParent(b);
            },
            set: function (c) {
                this.callParent(c);
            }
        }, {
            extend: 'B'
        });

        // Create object
        var obj = Esf.create('C', rand = random());

        // Test attributes
        test.equals(obj.a, rand, "Object attribute wasn't set by superclass constructor");
        test.equals(obj.b, 'test', "Defined attribute from superclass not found in object");
        test.equals(obj.d, 'test', "Defined attribute from childclass not found in object");

        // Test method invocation and return value
        test.equals(obj.foo(rand = random()), rand, "Object callParent method didn't returned expected value");

        // Test scope of methods
        obj.set(rand = random());
        test.equals(rand, obj.c, "Scope of callParent method wasn't the object");

        // Test instance of
        test.equals(obj instanceof B, true, "Class is not instanceof itself");
        test.equals(obj instanceof C, true, "Subclass is not an instanceof motherclass");

        test.done();
    },

    /**
     * Test singletons
     * @param test
     */
    testSingletons: function (test) {
        var rand = random();

        // Define singleton
        Esf.define('D', {
            b: rand
        }, {
            singleton: true
        });

        // Use singleton
        test.equals(Esf.getSingleton('D').b, rand, "Singleton was not created as expected");

        // Search for singleton in global scope
        test.equals(D.b, rand, "Singleton wasn't found in global scope");

        test.done();
    },

    testTags: function (test) {
        Esf.define('E', {
            foo: 'bar'
        }, {
            tags: ['test.tag']
        });

        // Find encoders
        var classes = Esf.findTaggedClasses('test.tag');

        // Test wether class is found by tag
        test.equals(E == classes[0], true, "Tagged Class wasn't found");

        test.done();
    },

    testFilterTags: function (test) {
        Esf.define('F', {
            foo: 'bar'
        }, {
            tags: [
                { name: 'test_filter.tag', bar: 'foo' }
            ]
        });

        // Find encoders
        var classes = Esf.findTaggedClasses('test_filter.tag'),
            filteredClasses = Esf.findTaggedClasses('test_filter.tag', { bar: 'foo' });

        // Test wether class is found by tag
        test.equals(F == classes[0], true, "Tagged Class wasn't found");

        // Test wether class is found by tag and filter
        test.equals(F == filteredClasses[0], true, "Tagged Class was found but not with a filter");

        test.done();
    },

    /**
     * Test private class definition
     *
     * @param test
     */
    testPrivateClass: function (test) {
        var rand = random(),
            Priv = Esf.define({
                b: rand
            });

        // Defining a private class
        test.equals((new Priv()).b, rand, "Defining private class failed");

        // Defining a nested private class
        Esf.define('Nested', function () {
            // Create private class prototype
            var PrivateException = Esf.define({
                constructor: function (msg) {
                    return this.callParent("Exception in Example: " + msg);
                }
            }, {
                extend: 'Error'
            });

            // Example
            return {
                foo: function () {
                    throw new PrivateException("bar");
                }
            };
        });

        test.throws(function () { new Nested().foo() } , null, "Private nested class couldn't be created");

        test.done();
    },

    testDeepClassInheritance: function (test) {
        var rand;

        // Define class
        Esf.define('G', {
            a: null,

            constructor: function (a) {
                // Save var
                this.a = a;
            },
            foo: function (b) {
                return b;
            }
        });

        // Define class
        Esf.define('H', {
            constructor: function (a) {
                this.callParent(a);
            },
            foo: function (b) {
                return this.callParent(b);
            }
        }, {
            extend: 'G'
        });

        // Define class
        Esf.define('I', {
            constructor: function (a) {
                this.callParent(a);
            },
            foo: function (b) {
                return this.callParent(b);
            }
        }, {
            extend: 'H'
        });

        // Create object
        var obj = Esf.create('I', rand = random());

        // Test attributes
        test.equals(obj.a, rand, "Object attribute wasn't set by super-superclass constructor");

        // Test method invocation and return value
        test.equals(obj.foo(rand = random()), rand, "Object callParent (deepth: 2) method didn't returned expected value");

        test.done();
    }
};