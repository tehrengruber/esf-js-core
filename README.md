This is a lightweight classloader that is heavily inspired by the Ext.ClassManager.

# Installation
Browser:
``` html
<script type="text/javascript" src="esf-core-web.js" />
```

Node-JS:
```
$ npm install esf-core
```

# Todo
- Add multiple inheritance
- Add interfaces
- Add private, protected, static methods
- Add namespace mapping
- Create more meanful error messages
- Add optional dependencies

# What it is (not)
This is not a try to rewrite a part of ExtJs, moreover it is to keep your code on a similar code basis, so you can move
easily. In fact, there is no advantage over ExtJs ClassManager at the moment, except of the License (Mit License).
Feel free to give suggestions or implement one of the many todos (multiple inheritance would be so nice).

If you want pure ExtJS functionality in NodeJS better use [ExtNode](https://github.com/brunotavares/extnode).

# Simple example
``` javascript
var Esf = require('esf-javascript-core');

Esf.define('A', {
    a: null,

    constructor: function (a) {
        // Save var
        this.a = a;

        // Heyho
        console.log('A');
    },
    foo: function (b) {
        console.log('foo - ' + b);
    }
});

Esf.define('B', {
    b: null,

    constructor: function (a, b) {
        // Call super constructor
        this.callParent(a);

        // Save var
        this.b = b;

        // Heyho
        console.log('B');
    },
    foo: function () {
        this.callParent('bar');
    }
}, {
    extend: 'A'
});

// Use
var b = new B(1, 2);

// or
var b = Esf.create('B', 1, 2);

/*
 * Output:
 * A
 * B
 * foo - bar
 */
b.foo();
```

# Singletons
``` javascript
Esf.define('Some.Namespace.Foo', {
    bar: function () {
        console.log('bar');
    }
}, {
    singleton: true
});

// Output: bar
Some.Namespace.Foo.bar();
```

# Tagged classes
This feature is quite new and the way of naming tags could be changed in future releases. It was mainly inspired by
[Symfony's tags](http://symfony.com/doc/2.0/book/service_container.html#tags) in services.

``` javascript
Esf.define('FooJsonEncoder', {
    supports: function (format) {
        return format == 'json';
    },
    deserialize: function (string, reviver) {
        return JSON.parse(string, reviver);
    },
    serialize: function (data, reviver) {
        return JSON.stringify(data, reviver);
    }
}, {
    tags: ['esf_serializer.encoder'],
    singleton: true
});

// Find encoders
var encoders = Esf.findTaggedClasses('esf_serializer.encoder'),
    result;

if (encoders.length == 0)
    throw "no encoders were found";

for (var key in encoders) {
    var encoder = encoders[key];

    if (encoder.supports('json')) {
        result = encoder.deserialize('{"success":true}');
        break;
    }
}

// outputs true
console.log(result.success);
```

Tagged classes are all fine but sometimes you also want to filter them a bit more. See this example.

``` javascript
Esf.define('Bar', {
    foo: 'bar'
}, {
    tags: [
        { name: 'test.tag' }
    ]
});

Esf.define('Foo', {
    foo: 'bar'
}, {
    tags: [
        { name: 'test.tag', bar: 'foo' }
    ]
});

// Find encoders
var classes = Esf.findTaggedClasses('test.tag'),
    filteredClasses = Esf.findTaggedClasses('test.tag', { bar: 'foo' });

/*
 * Outputs:
 * 2
 * 1
 */
console.log(classes.length);
console.log(filteredClasses.length);
```

# Private classes
``` javascript
Esf.define('Example', {
    foo: function () {
        var Exception = Esf.define({
            constructor: function (msg) {
                return this.callParent("Exception in Example: " + msg);
            }
        }, {
            extend: 'Error'
        });

        throw new Exception("bar");
    }
});

new Example().foo();
```

# Unit Tests
```
$ nodeunit test.js
```

Expected output:
```
test.js
✔ testClassDefinition
✔ testClassInheritance
✔ testSingletons
✔ testPrivateClass

OK: 14 assertions (7ms)
```

# Different development environments
Because javascript is available for both client (browser) and server side (e.g. node) you may face the problem to keep
your code base clean from eviromental specific code. To accommodate this problem it is handy to overwrite classes
in environmental specific subpackages.

Package foo:
``` javascript
Esf.define('Foo', {
    bar: function () {
        this.doSomeStuff = true;
    }
});
```

Package foo-bar (must have same class scope as foo)
``` javascript
Esf.define('Foo', {
    bar: function () {
        this.doSomeStuff = true;
    }
});
```

It is possible to overwrite classes in the same package, but this will break any directory structure and support for
autoloaders (especially in browsers).

# Roadmap
- Class Autoloader
- JsBuilder
- Amd Examples (requirejs, almond)

# License
(The MIT License)

Copyright (C) 2012 Till Ehrengruber

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.